# Endpoints

- port: `:5000`
- prefix: `/api/v1`

## Misc

| method | Pattern | Code |
|--------|---------|------|
| GET    | /       | 200  |
| POST   | /health | 201  |

## Referees

| method | Pattern        | Code |
| ------ | -------------- | ---- |
| GET    | /referees      | 200  |
| POST   | /referees      | 201  |
| GET    | /referees/{id} | 200  |
| PUT    | /referees/{id} | 200  |
| DELETE | /referees/{id} | 204  |

## Teams

| method | Pattern     | Code |
| ------ | ----------- | ---- |
| GET    | /teams      | 200  |
| POST   | /teams      | 201  |
| GET    | /teams/{id} | 200  |
| PUT    | /teams/{id} | 200  |
| DELETE | /teams/{id} | 204  |

## Leagues

| method | Pattern       | Code |
| ------ | ------------- | ---- |
| GET    | /leagues      | 200  |
| POST   | /leagues      | 201  |
| GET    | /leagues/{id} | 200  |
| PUT    | /leagues/{id} | 200  |
| DELETE | /leagues/{id} | 204  |

## Matches
