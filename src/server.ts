/*
 *  apito-elysia is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  apito-elysia is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with apito-elysia. If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

import { app } from "./app";

export const HELLO: string = "Welcome, Apito.";
const PORT: number = 8080;
app
  .listen(PORT, () => {
    console.log(
      `🦊 Elysia is running at ${app.server?.hostname}:${app.server?.port}`,
    );
  })
  .on("error", (e) => {
    console.log(e);
  });
