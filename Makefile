# apito-elysia is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# apito-elysia is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with apito-elysia. If not, see <https://www.gnu.org/licenses/>.

# DEPENDENCIES: podman, gawk, fzf, guix.

# LOAD ENV FILES
-include envs/.env.*

NAME := apito-elysia
VERSION := $(shell awk '/version/ {line=substr($$2, 2,5); print line}' ./package.json)
RUNNER ?= podman
BACKEND_IMAGE=${GITLAB_REGISTRY}/${USER}/${NAME}:${VERSION}
BACKEND_FOLDER=/app

# ------------------------------------
base: initial database #server

stats:
	${RUNNER} pod stats ${POD_NAME}

stop:
	${RUNNER} pod rm --force --ignore ${POD_NAME}
	${RUNNER} container rm --force --ignore ${NAME}
	${RUNNER} container rm --force --ignore ${DATABASE_NAME}
	${RUNNER} volume rm --force ${DATABASE_DATA}

initial:
	# ----------- CREATE POD
	${RUNNER} pod create \
		--publish ${FRONTEND_PORT}:${FRONTEND_INTERNAL_PORT} \
		--publish ${BACKEND_PORT}:${BACKEND_INTERNAL_PORT} \
		--name ${POD_NAME}

database:
	# ----------- ADD DATABASE
	${RUNNER} rm -f ${DATABASE_NAME}
	${RUNNER} run ${RUNNER_STATS} \
		--detach \
		--pod ${POD_NAME} \
		--name ${DATABASE_NAME} \
		--env POSTGRES_PASSWORD=${SQL_PASSWORD} \
		--env POSTGRES_USER=${SQL_USERNAME} \
		--env POSTGRES_DB=${SQL_DATABASE} \
		--volume ${DATABASE_DATA}:${SQL_DATA}:Z \
		${DATABASE_IMAGE}

server:
	# ----------- SERVER
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--detach \
		--restart=unless-stopped \
		--name ${SERVER_NAME} \
		--volume ${PWD}:${BACKEND_FOLDER}:Z \
		--volume ./ops/nginx-default.conf:/etc/nginx/conf.d/default.conf:Z \
		${SERVER_IMAGE}

prod:
	${RUNNER} container rm -f ${NAME}-prod
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm -it \
		--name ${NAME}-prod \
		--volume ${PWD}:/app \
		--workdir /app \
		${BACKEND_IMAGE}

start:
	${RUNNER} container rm -f ${NAME}-start
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm -it \
		--name ${NAME}-start \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env SQL_ENGINE=${SQL_ENGINE} \
		--env DATABASE_URL=${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE} \
		${BACKEND_IMAGE} \
		bash -c 'bun run start:dev'

tests:
	${RUNNER} container rm -f ${NAME}-test
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm -it \
		--name ${NAME}-test \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env SQL_ENGINE=${SQL_ENGINE} \
		--env DATABASE_URL=${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE} \
		${BACKEND_IMAGE} \
		bash -c 'bun tests'

repl:
	${RUNNER} container rm -f ${NAME}-repl
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm -it \
		--name ${NAME}-repl \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env SQL_ENGINE=${SQL_ENGINE} \
		--env DATABASE_URL=${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE} \
		${BACKEND_IMAGE} \
		bash

repl-db:
	# ----------- DATABASE REPL
	${RUNNER} exec -it ${DATABASE_NAME} mongosh \
		-u ${POSTGRES_USER} \-p ${POSTGRES_PASSWORD}

command:
	${RUNNER} container rm -f ${NAME}-command
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm -it \
		--name ${NAME}-command \
		--volume ${PWD}:/app:Z \
		--workdir /app \
		--env SQL_ENGINE=${SQL_ENGINE} \
		--env DATABASE_URL=${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE} \
		${BACKEND_IMAGE} \
		bash -c 'bun $(shell cat container-commands | fzf)'

build:
	# ---------------------- BUILD BACKEND IMAGE
	${RUNNER} build \
		--file ./Dockerfile \
		--tag ${GITLAB_REGISTRY}/${USER}/${NAME}:${VERSION}

publish:
	# ---------------------- PUBLISH BACKEND IMAGE
	${RUNNER} push ${BACKEND_IMAGE}

.PHONY: repl repl-db build publish stop base stats prod start tests command
.DEFAULT_GOAL := tests
